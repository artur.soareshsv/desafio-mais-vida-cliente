import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Medico } from './../model/medico';

@Injectable()
export class MedicoService {
  private URL_ESPECIALIDAE = 'http://localhost:8080/medico';

  constructor(private http: Http) { }

  save(medico: Medico): Observable<Medico> {
    return this.http.post(this.URL_ESPECIALIDAE, medico)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

  getMedicos(): Observable<Medico[]> {
    return this.http.get(this.URL_ESPECIALIDAE)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

  getMedico(id): Observable<Medico> {
    return this.http.get(this.URL_ESPECIALIDAE + '/' + id)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json() || 'Server error'));
  }

}
