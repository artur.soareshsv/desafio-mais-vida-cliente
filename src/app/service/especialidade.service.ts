import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { SelectItem } from 'primeng/primeng';
import { Observable } from 'rxjs';
import { Categoria } from './../model/categoria';

@Injectable()
export class EspecialidadeService {
  private URL_ESPECIALIDAE = 'http://localhost:8080/especialidade';

  constructor(private http: Http) { }

  getEspecialidades(): Observable<SelectItem[]> {
    return this.http.get(this.URL_ESPECIALIDAE)
      .map(res => res.json())
      .map((especialidades: Categoria[]) => {
        const values: SelectItem[] = [];
        especialidades.forEach(especialidade => {
          values.push({ label: especialidade.nome, value: { id: especialidade.id, nome: especialidade.nome } });
        });
        return values;
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
