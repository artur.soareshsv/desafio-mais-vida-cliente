import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { Observable } from 'rxjs';

import { Estado } from './../model/estado';
import { Municipio } from './../model/municipio';

@Injectable()
export class LocalidadeService {
  private URL_LOCALIDADE = 'http://localhost:8080/localidade/';

  constructor(private http: Http) { }

  getEstados(): Observable<SelectItem[]> {
    return this.http.get(this.URL_LOCALIDADE + 'estados')
      .map(res => res.json())
      .map((estados: Estado[]) => {
        const values: SelectItem[] = [];
        estados.forEach(estado => {
          values.push({ label: estado.nome, value: { id: estado.id, nome: estado.nome, uf: estado.uf } });
        });
        return values;
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getMunicipios(idEstado: number): Observable<SelectItem[]> {
    return this.http.get(this.URL_LOCALIDADE + 'municipios/' + idEstado)
      .map(res => res.json())
      .map((municipios: Municipio[]) => {
        const values: SelectItem[] = [];
        municipios.forEach(municipio => {
          values.push({ label: municipio.nome, value: { id: municipio.id, nome: municipio.nome, estado: municipio.estado } });
        });
        return values;
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
