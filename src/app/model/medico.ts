import { Categoria } from './categoria';
import { Municipio } from './municipio';
import { Status } from 'app/model/status';

export class Medico {
    id?: number;
    nome: string;
    sobrenome: string;
    email: string;
    ativo: boolean;
    status: Status;
    municipio: Municipio;
    categoria: Categoria;

    constructor() {
        this.nome = '';
        this.sobrenome = '';
        this.email = '';
        this.ativo = false;
        this.status = new Status();
        this.municipio = new Municipio();
        this.categoria = new Categoria();
    }
}
