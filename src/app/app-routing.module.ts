import { SelectivePreloadingStrategy } from './selctive-preloading-strategy';
import { ListagemComponent } from './view/medico/listagem/listagem.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { AppComponent } from './app.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'medico', pathMatch: 'full' },
    { path: 'medico', redirectTo: 'medico', pathMatch: 'full' },
    { path: 'medico/novo', redirectTo: 'medico/novo', pathMatch: 'full' },
    { path: 'medico/:id', redirectTo: 'medico/:id', pathMatch: 'full' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            { preloadingStrategy: SelectivePreloadingStrategy }
        )
    ],
    exports: [
        RouterModule
    ],
    providers: [
        SelectivePreloadingStrategy
    ]
})
export class AppRoutingModule { }
