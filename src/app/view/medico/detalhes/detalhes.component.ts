import { Route, Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Medico } from './../../../model/medico';
import { MedicoService } from './../../../service/medico.service';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.css'],
  providers: [MedicoService]
})
export class DetalhesComponent implements OnInit {
  medico: Medico;

  constructor(private service: MedicoService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadMedico();
  }

  private loadMedico() {
    this.medico = new Medico();

    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      if (id) { this.service.getMedico(id).subscribe(medico => this.medico = medico); }
    });
  }

}
