import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListagemComponent } from './listagem/listagem.component';
import { CadastrarComponent } from './cadastrar/cadastrar.component';
import { DetalhesComponent } from './detalhes/detalhes.component';

const medicoRoutes: Routes = [
    { path: 'medico', component: ListagemComponent },
    { path: 'medico/novo', component: CadastrarComponent },
    { path: 'medico/:id', component: DetalhesComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(medicoRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class MedicoRoutingModule { }
