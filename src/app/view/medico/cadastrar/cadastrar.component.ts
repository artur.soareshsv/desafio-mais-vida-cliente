import { Router } from '@angular/router';
import { Status } from 'app/model/status';
import { Component, OnInit } from '@angular/core';
import { SelectItem, Message } from 'primeng/primeng';

import { Estado } from './../../../model/estado';
import { Medico } from './../../../model/medico';
import { LocalidadeService } from './../../../service/localidade.service';
import { Municipio } from './../../../model/municipio';
import { EspecialidadeService } from './../../../service/especialidade.service';
import { MedicoService } from './../../../service/medico.service';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css'],
  providers: [LocalidadeService, EspecialidadeService, MedicoService]
})
export class CadastrarComponent implements OnInit {

  medico: Medico = new Medico();
  estados: SelectItem[];
  municipios: SelectItem[];
  especialidades: SelectItem[];
  selectedValue: string[] = [];
  msgs: Message[] = [];

  constructor(
    private localidadeService: LocalidadeService,
    private especilidadeService: EspecialidadeService,
    private medicoService: MedicoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadEspecialidades();
    this.loadEstados();
    this.loadMunicipios(null);
  }

  onEstadoChange(estado: Estado) {
    this.loadMunicipios(estado);
  }

  save() {
    this.medico.ativo = this.selectedValue.length > 0 ? true : false;
    this.medicoService.save(this.medico).subscribe(medico => {
      this.msgs.push({ severity: 'success', summary: 'Operação realizada com sucesso', detail: '' });
      setTimeout(() => {
        this.router.navigate(['/medico']);
      }, 1000);
    });
  }

  private loadEspecialidades() {
    this.especialidades = [];
    this.especialidades.push({ label: 'Selecione uma especialidade', value: null });

    this.especilidadeService.getEspecialidades().subscribe(especialidades => {
      this.especialidades = this.especialidades.concat(especialidades);
    });
  }

  private loadEstados() {
    this.estados = [];
    this.estados.push({ label: 'Selecione um estado', value: null });

    this.localidadeService.getEstados().subscribe(estados => {
      this.estados = this.estados.concat(estados);
    });
  }

  private loadMunicipios(estado: Estado) {
    this.municipios = [];
    this.municipios.push({ label: 'Selecione um municipio', value: null });

    if (estado) {
      this.localidadeService.getMunicipios(estado.id).subscribe(municipios => {
        this.municipios = this.municipios.concat(municipios);
      });
    }
  }
}
