import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LightboxModule } from 'primeng/primeng';

import { Medico } from './../../../model/medico';
import { MedicoService } from './../../../service/medico.service';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css'],
  providers: [MedicoService]
})
export class ListagemComponent implements OnInit {
  medicos: Medico[] = [];
  selectedValue: string[] = [];

  constructor(private service: MedicoService, private router: Router) { }

  ngOnInit() {
    this.service.getMedicos().subscribe(medicos => this.medicos = medicos);
  }

  viewMedico(medico: Medico) {
    this.router.navigate(['/medico/', medico.id]);
  }

}
