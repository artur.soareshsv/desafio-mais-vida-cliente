import { DesafioMaisVidaClientePage } from './app.po';

describe('desafio-mais-vida-cliente App', () => {
  let page: DesafioMaisVidaClientePage;

  beforeEach(() => {
    page = new DesafioMaisVidaClientePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
